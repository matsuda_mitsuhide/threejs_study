var renderer;
var canvas;
var container = document.getElementById('app');
var stats;
var scene;
var camera;
var cameraCtrl;
var light;

function initRenderer() {
  renderer = new THREE.WebGLRenderer({
    antialias: true
  });
  renderer.setClearColor(0xffffff, 1);

  canvas = renderer.domElement;
  container.appendChild(canvas);

  stats = new Stats();
  container.appendChild(stats.dom);
}

function initScene() {
  scene = new THREE.Scene();
  scene.add(new THREE.AxisHelper(100));
}

function initCamera() {
  camera = new THREE.PerspectiveCamera(45, 1, 0.1, 1000);
  camera.position.set(10, 10, 30);
  cameraCtrl = new THREE.OrbitControls(camera, canvas);
}

function initLight() {
  light = new THREE.DirectionalLight(0xffffff);
  light.position.set(0, 0.5, 2);
  scene.add(light);

  scene.add(new THREE.AmbientLight(0xffffff, 0.2));
}

function resize() {
  var width = container.clientWidth;
  var height = container.clientHeight;
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function render() {
  requestAnimationFrame(render);
  stats.begin();
  renderer.render(scene, camera);
  stats.end();
}

initRenderer();
initScene();
initCamera();
initLight();

window.addEventListener('resize', resize, false);
resize();
render();


function main() {
  var material = new THREE.MeshPhongMaterial({ color: '#f00' });
  var geometry = new THREE.BoxBufferGeometry(1, 1, 1);

  var obj = new THREE.Object3D();
  obj.position.x = 0;
  scene.add(obj);

  for (var objY = 0; objY < 10; objY += 1) {
    for (var objX = 0; objX < 10; objX += 1) {
      var obj = new THREE.Object3D();
      obj.position.x = objX * 20;
      obj.position.y = objY * 20;
      scene.add(obj);

      for (var z = 0; z < 10; z += 1) {
        for (var y = 0; y < 10; y += 1) {
          for (var mesh, x = 0; x < 10; x += 1) {
            var mesh = new THREE.Mesh(geometry, material);
            mesh.position.x = x * 2;
            mesh.position.y = y * 2;
            mesh.position.z = z * 2;
            obj.add(mesh);
          }
        }
      }
    }
  }
}

main();

