var renderer;
var canvas;
var container = document.getElementById('app');
var stats;
var scene;
var camera;
var cameraCtrl;
var light;

function initRenderer() {
  renderer = new THREE.WebGLRenderer({
    antialias: true
  });
  renderer.setClearColor(0xffffff, 1);

  canvas = renderer.domElement;
  container.appendChild(canvas);

  stats = new Stats();
  container.appendChild(stats.dom);
}

function initScene() {
  scene = new THREE.Scene();
  scene.add(new THREE.AxisHelper(100));
}

function initCamera() {
  camera = new THREE.PerspectiveCamera(45, 1, 0.1, 1000);
  camera.position.set(10, 10, 30);
  cameraCtrl = new THREE.OrbitControls(camera, canvas);
}

function initLight() {
  light = new THREE.DirectionalLight(0xffffff);
  light.position.set(0, 0.5, 2);
  scene.add(light);

  scene.add(new THREE.AmbientLight(0xffffff, 0.2));
}

function resize() {
  var width = container.clientWidth;
  var height = container.clientHeight;
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function render() {
  requestAnimationFrame(render);
  stats.begin();
  renderer.render(scene, camera);
  stats.end();
}

initRenderer();
initScene();
initCamera();
initLight();

window.addEventListener('resize', resize, false);
resize();
render();

function createStar(r, count, segments) {
  var geometry = new THREE.BoxBufferGeometry( 1, 1, 1 );
  var material = new THREE.MeshPhongMaterial({
    color: "#f00"
  });

  var vertexList = [];
  for (var i = 0; i < count; i++) {
    var theta = 2 * i / count * Math.PI + Math.PI / 2;
    vertexList.push([
      r * Math.cos(theta),
      r * Math.sin(theta)
    ]);
  }

  for (var n = 0; n < vertexList.length; n ++) {
    var target1 = vertexList[n];
    var target2 = vertexList[(n + Math.floor((count - 1) / 2)) % vertexList.length];
    for (var m = 0; m < segments; m++) {
      var mesh = new THREE.Mesh(geometry, material);
      mesh.position.set( (target1[0] - target2[0]) / segments * m + target2[0], (target1[1] - target2[1]) / segments * m + target2[1], 0);
      scene.add(mesh);
    }
  }
}

function main() {
  createStar(20, 10, 15);
}

main();